import bpy

from ..controllers.check_addons import check_multi

# Import all variable setup
from ..models.paths import SbsModelsSetup


# -----------------------------------------------------------------------------
# Settings for this addons
# -----------------------------------------------------------------------------
class SubstanceSettings(bpy.types.AddonPreferences):
    """
    This class build the view to setup the addon inside blender.
    The can show :
        - The path string to load Substance Painter.
        - A simple operator to check the path.
        - A warning about the multi uv editing tools.
    """
    bl_idname = "SubstanceBridge"

    # All software path.
    path_painter = SbsModelsSetup.path_painter

    def draw(self, context):
        layout = self.layout
        row = layout.row(align=False)
        row.alignment = 'LEFT'
        row.label(text="Add-ons needed :")

        ops = "debug.check_multi_uv"
        row.operator(ops, text='Multi UV')
        ops = "debug.check_gltf"
        row.operator(ops, text='GLTF')

        row = layout.row()
        box = row.box()
        box.label(text="Substance Path.")
        box.prop(self, "path_painter")
        box.operator("substance.check", text="Check").path = self.path_painter

        # layout.prop(self, "path_batchtools")
        # layout.prop(self, "path_designer")


def register():
    bpy.utils.register_class(SubstanceSettings)


def unregister():
    bpy.utils.unregister_class(SubstanceSettings)
